import { Component } from '@angular/core';
import { faUser, faClock, faThLarge, faShoppingCart, faCompass} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'vyrent';
  faCompass = faCompass;
  faUser = faUser;
  faClock = faClock;
  faThLarge = faThLarge;
  faShoppingCart = faShoppingCart;
}
