import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { faArrowLeft, faSearch } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  products = [];
  searchText;
  faArrowLeft = faArrowLeft;
  faSearch = faSearch;
  placeholder = true;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getList().subscribe((data: any[])=>{  
			this.products = data;  
		})
  }
  hidePlaceholder(){
    document.getElementById('search-input').focus();
    this.placeholder = false;
  }

  showPlaceholder(val){
    if(!val)
      this.placeholder = true;
  }

}
