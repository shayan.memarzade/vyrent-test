import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  id$;
  product: any = {};
  faTimes = faTimes;
  seeMore: boolean = false;
  details = [
    { title: 'Brand', name: "brand"},
    { title: 'Bezal', name: "bezel"},
    { title: 'Band', name: "band_type"},
    { title: 'Case Diameter', name: "case_diameter"},
    { title: 'Band Material', name: "band_material"},
    { title: 'Case Material', name: "case_material"}]
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService) { }

  ngOnInit(): void {
    this.id$ = this.route.snapshot.params['id'];
    this.apiService.getDetail(this.id$).subscribe((data: any[]) => {
      this.product = data;
    })
  }

}
