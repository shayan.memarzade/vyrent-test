import { Component,ViewEncapsulation, OnInit, Input} from '@angular/core';
import {SwiperOptions} from 'swiper';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SliderComponent implements OnInit {
  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    slidesPerView: 1
  };
  @Input() slides: {text: string}[]
  constructor() { }
  ngOnInit(): void {}

}
